////
 * Copyright (C) Eclipse Foundation, Inc. and others. 
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-FileType: SOURCE
 *
 * SPDX-FileCopyrightText: 2015 Eclipse Foundation, Inc.
 * SPDX-FileCopyrightText: 2015 Contributors to the Eclipse Foundation
 *
 * SPDX-License-Identifier: EPL-2.0
////

[[contributing]]
= Contributing to {aForgeName} Project

[[contributing-contributors]]
== Contributors

Anybody can be a contributor. To be a contributor, you just need to contribute. Contributions typically take the form of code or documentation submitted to the project's source code repository, but may also take the form of answering questions in project and community forums, and more.

For contributions that contain intellectual property (e.g., source code or documentation), some formal documentation is required to ensure that the rights associated with the intellectual property are properly managed and licensed.

To contribute to {aForgeName} open source project, a contributor must:

. Create an <<contributing-account,Eclipse Foundation Account>>;
. Sign the <<contributing-eca,Eclipse Contributor Agreement>>;
. Read the project's contribution guide to learn about coding style and other guidelines for contributions (contribution guidelines are typically found a `CONTRIBUTING` or `README` file in the root of every source code repository);
. Create a commit:
 * Ensure that the author credentials on the commit record match the email address associated with their Eclipse Foundation Account
. Create a pull request (GitHub), merge request (GitLab), or submit a review (Gerrit); and
. Work with the project team to merge the contribution.

Contributors retain ownership of their contributions; the ECA, for example, states in part:

____
This ECA, and the license(s) associated with the particular Eclipse Foundation projects You are contributing to, provides a license to Your Contributions to the Eclipse Foundation and downstream consumers, but *You still own Your Contributions*, and except for the licenses provided for in this ECA, You reserve all right, title and interest in Your Contributions. 
____

That is, the contributor owns their contributions, but grants a license to the Eclipse Foundation and downstream consumers to use them under the terms of the project license.

[graphviz, images/contributor-contributors, svg]
.Becoming a contributor
----
digraph {
	// Graph properties
	bgcolor=transparent
	
    graph [ranksep="0.25", nodesep="0.25"]
    
	// Nodes that define the key points in the process
	node [shape=box;style=filled;fillcolor=white;fontsize=12;group=g1]
	
	start [label="You want to be\na contributor"; style="filled,bold"];
	eca [label=<<b>You</b> sign<br/>the ECA>];
	contribute [label=<Create pull requests>];
	committer [label="Project Team elects\nyou as a committer"; style="filled,bold"];
	
	node [shape=diamond;style=filled;fillcolor=white;fontsize=10;group=g1];
	
	merit [label="Significant\ncontributor?"];
		
	start -> eca -> contribute;
	contribute:e -> merit:e;
	merit -> committer [label="Yes"];
	merit:w -> contribute:w [xlabel="No"];
}
----

[NOTE]
====
{forgeName} committers must engage with the EMO Intellectual Property Team to review your <<ip-project-code, project code>> contributions, or <<ip-third-party, third-party>> content that your contributions leverage. The committers will take are of this, but may need your help as part of the process.
====

After establishing a pattern of contributing high quality contributions a contributor may be invited to join the project as a committer; an existing project committer will nominate a contributor to be a committer via <<elections-committer, committer election>>.

[NOTE]
====
In order to grant the ability to assign issues and merge/pull requests to a non-committer contributor on repositories that are hosted either on GitLab or GitHub, a <<contributing-committers,committer>> must designate the individual as a contributor by providing their email address in the <<pmi-contributors,Contributors field>> in the project's metadata (PMI).
====

[[contributing-committers]]
== Committers

For {forgeName} projects (and the open source world in general), committers are the ones who hold the keys. Committers decide what code goes into the code base, they decide how a project builds, and they ultimately decide what gets delivered to the adopter community. With awesome power, comes awesome responsibility, and so the Open Source Rules of Engagement described by the {edpLink}, puts _meritocracy_ on equal footing with _transparency_ and _openness_: becoming a committer isn’t necessarily hard, but it does require a demonstration of merit.

In practical terms, there are two ways to become {aForgeName} Committer. 

The first way is to be listed as an initial committer on a new <<starting-proposal,project proposal>>. When projects come to the Eclipse Foundation we need them to actually start with committers, and so we include this as part of the bootstrapping. As part of the process of community vetting a new project proposal, the committers listed are themselves vetted by the community. In effect, the project proposal process also acts as a committer election that’s open to the entire community.

The second way to become a committer is to get voted in via <<elections-committer,committer election>>. After an individual has made a small number of high quality contributions that demonstrate that they understand how the project works, understand the {edpLink} and are prepared to implement the {ipDueDiligenceLink}, a committer will invite them to join the team and initiate the election. Committer elections start with a nomination by an existing committer that includes a statement of merit that usually takes the form of a list various contributions that the individual has made to the project. What constitutes a sufficient demonstration of merit varies by project team.

[NOTE]
====
Committer status is assigned on a project-by-project basis. That is, individuals have committer rights only on those projects for which they hold committer status. For all other projects, they are contributors.
====

When a contributor becomes a committer, they must be covered by a committer agreement (either a <<paperwork-mcca,Member Committer and Contributor Agreement>> (MCCA) or an <<paperwork-ica,Individual Committer Agreement>> (ICA). Committers on specification projects must be covered by <<specifications-agreements, additional agreements>>. As the last step in the committer election process, the Eclipse Foundation systems will determine which agreement applies and engage a <<paperwork,committer paperwork>> workflow with the new committer to ensure that everything is in place.

[[contributing-account]]
== Eclipse Foundation Account

Eclipse Foundation project resources are accessible without an account. That is, members of the community may browse code, documentation, issues, help, etc. without creating an Eclipse Foundation Account. For those members of the community that wish to contribute code patches, open issues, add content to wikis, etc., an {accountUrl}[Eclipse Foundation Account] is required.

For contributors and committers, their Eclipse Foundation account is their primary portal into their relationship with the Eclipse Foundation. From their account page, individuals can sign the <<contributing-eca, Eclipse Contributor Agreement>>, manage their mailing list subscriptions, access build resources, and more.

[TIP]
====
If you're working with projects that use GitHub, be sure to add your GitHub Id to your Eclipse Foundation Account so that our scripts can add you to the GitHub Teams that we use to manage access to Eclipse Foundation repositories hosted on GitHub. When you become a committer on an Eclipse project that hosts their source code repositories on GitHub, you will be sent an invitation to join the GitHub Team associated with that project.
====

[[contributing-eca]]
== Eclipse Contributor Agreement

[[resources-eca]]The purpose of the Eclipse Contributor Agreement (ECA) is to provide a written record that contributors have agreed to provide their contributions of code and documentation under the licenses used by the Eclipse project(s) they’re contributing to. It also makes it clear that contributors are promising that what they are contributing to Eclipse is code that they wrote, and that they have the necessary rights to contribute it to Eclipse projects. And finally, it documents a commitment from the contributor that their open source contributions will be permanently on the public record.

All contributors, who are not committers on the {forgeName} project, must sign the {ecaUrl}[Eclipse Contributor Agreements] (ECA) to ensure that the necessary <<ip,intellectual property>> (IP) rights to the contribution are granted.

Contributors who are already covered by a committer agreement (either an MCCA or ICA) are not required to also sign the ECA.

[TIP]
====
You can determine your ECA status from your {accountUrl}[Eclipse Foundation Account page]. Your ECA status is shown in the top-right corner. 
==== 

Committers *do not* require an ECA to contribute to a project on which they have committer status.

[[contributing-faq]]
== Frequently Asked Questions

[qanda]
Is there any requirement on what is necessary for committers/project leads to consider when dealing with contributions from someone from the same company? ::

No requirement. Getting reviews/approvals from any committer on your project is sufficient, whether they are from the same company or different.
+
Project teams may establish their own rules regarding how contributions are accepted. These rules range from allowing any committer to approve a contribution, to requiring that contributions from a committer be approved by another committer, to requiring that all contributions be approved by a project lead. Whatever the case, it should be documented (typically captured in a `CONTRIBUTING` file).
+
It is generally considered good practice (or a nice-to-have) to request a review from an "outsider", especially if the proposed change might be considered complex or controversial.  It's always good to get outside viewpoints. But the specific rules are left for the project team to decide.

Do I need to sign the Eclipse Contributor Agreement (ECA)? ::

If you are not a committer on a particular Eclipse open source project, to contribute (e.g., make pull requests) to that project, you need to be covered by an ECA. If you are already a committer on any Eclipse open source project, then you are already covered by an ECA through your committer agreement and do not need to sign the ECA separately. If your employer has signed the <<paperwork-mcca,Member Committer and Contributor Agreement>>, then you are already covered by an ECA and you do not need to sign the ECA separately.
+
If you are a committer on a project you do not need an ECA to contribute to that project. 
+
Assuming that you are empowered to do so, you can individually sign the ECA even if you are already covered by another agreement.

How do I know if I’m covered by an Eclipse Contributor Agreement? ::

Visit your Eclipse Account page. Your <<contributing-eca, Eclipse Contributor Agreement>> (ECA) status is shown in the top-right corner.
+
For example, this contributor is covered by the ECA (and is a committer on one or more Eclipse Foundation open source projects).
+
image::images/accounts-status.png[]

How do I know if somebody else is covered by an Eclipse Contributor Agreement (ECA)? ::

Visit your <<contributing-account,Eclipse Foundation Account>> page and click on "Eclipse Contributor Agreement" in the _Status_ block. Use the ECA Validation Tool on _My Eclipse Contributor Agreement_ page to verify the ECA status of an individual using their email address.
+
image::images/eca_validation_tool.png[]

How do I know if my employer is a member of the Eclipse Foundation? ::

If your employer is a member of the Eclipse Foundation, they will be listed on the {memberUrl}[Explore Our Members] page.

How do I know if my employer has signed the Member Committer and Contributor Agreement (MCCA)? ::

Check with your manager. If your manager doesn't know, you can contact the {emoRecordsEmailLink} for assistance.

I know that my employer has signed the Member Committer and Contributor Agreement (MCCA), but my account page shows that I am not covered by the ECA. What should I do? ::

It's likely that you haven't provided us with employer information. In order for our systems to know that you are affiliated with a particular organization, you must specify your employer in your <<contributing-account, Eclipse Foundation Account>>.

I am a committer on a project, but I cannot push to that project's GitHub repositories. What should I do? ::

It's likely that you haven't provided us with your GitHub Id. Our systems manage the composition of the GitHub teams that have the necessary privileges on project repositories. For that, we need your GitHub Id, which you must provide us with in your <<contributing-account, Eclipse Foundation Account>>.
