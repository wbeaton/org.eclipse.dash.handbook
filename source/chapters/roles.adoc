////
 * Copyright (C) 2020 Eclipse Foundation, Inc. and others. 
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-FileType: SOURCE
 *
 * SPDX-FileCopyrightText: 2015 Eclipse Foundation, Inc.
 * SPDX-FileCopyrightText: 2015 Contributors to the Eclipse Foundation
 *
 * SPDX-License-Identifier: EPL-2.0
////

[[roles]]
= Project Roles

TBD

[[roles-cm]]
== Committers

For {forgeName} projects (and the open source world in general), committers are the ones who hold the keys. Committers decide what code goes into the code base, they decide how a project builds, and they ultimately decide what gets delivered to the adopter community. With awesome power, comes awesome responsibility, and so the Open Source Rules of Engagement described by the {edpLink}, puts _meritocracy_ on equal footing with _transparency_ and _openness_: becoming a committer isn’t necessarily hard, but it does require a demonstration of merit.

Operate in an open, transparent, and meritocratic manner.

Write code. Code written by a committer should be pushed directly into the project's source code repository.

Review contributions from contributors. Contributions

[NOTE]
====
The IP due diligence process refers to content that is written "under the supervision of the PMC". We regard this to mean content that has been developed in an open and transparent manner. That is, content that is in-scope and in-plan. Generally, this means that the content is developed in response to a documented issue or plan item, or is otherwise a natural evolution of the code developed in manner that allows participation by the community.

A committer who spends a few hours developing content before pushing it directly into the project's source code repository is likely working "under the supervision of the PMC"; a committer who _disappears_ for a few weeks to work on a feature and then drops it into the project repository is not. In the former example, the hypothetical committer is working in a manner that faciliates participation by others; the later hypothetical committer is not. 

It is extremely difficult to attract contributors to participate in an open source project when massive changes are held back and then dumped.
====


[[roles-pl]]
== Project Lead

The short version is that the project lead is the first link in the project leadership chain and is the primary liaison between the project team and the EMO. 

A project lead is not a super committer or anything like that. Committer and project lead are two different roles. A project lead is almost always also a committer. Most privileges on an Eclipse open source project are given to committers. I say most privileges, because we're always trying to sort out how to give project leads more privileges in GitHub and GitLab (e.g., Bug 533471).

They are responsible for ensuring that project committers are following the rules. At the most basic level, "the rules" are the open source rules of engagement defined in the EDP (openness, transparency, and meritocracy), and we depend on them to make sure that team members understand their obligations under the Eclipse IP Policy (and seek help/advice from the PMC and EMO when they need assistance).

I don't generally expect that anybody has read the IP Policy. I agree that it is cruel and unusual punishment. We describe the various practices with regard to the IP Policy in the handbook; we need the project leads to help us make sure that these practices are followed. 

When committers are disruptive or otherwise behaving in a manner that is detrimental to the project, the project lead should work with the PMC and the EMO to resolve the issue. As you've noted, the project lead does have the ability to retire committers. This is a power that must be used responsibly. The project lead needs to be able to support their decision to retire a committer. As a best practice, the project lead should give committers reasonable opportunity to retrain their status, and--should the decision be made to retire--committer retirements should be announced on the project's mailing list. The six months of inactivity is more of a suggested period of time for considering retirement of committers; we defer to the judgement of the project lead to determine when and if somebody who is inactive should be retired.

The project lead is not the technical lead. So it's not strictly their responsibility to develop things like coding standards, project policies, and such. They are responsible, however, for making sure that those sorts of things get created and for making sure that the project team is working to complete shared goals. We have no formal designation of technical lead; this tends to occur organically.

Any committer can initiate a review with the EMO, but we always copy the project lead(s) in related communication to ensure that they are aware that the committer is acting as their delegate.

Regarding the assertion that the IP Policy has been followed... this happens when the project lead (or their delegate) submits the IP Log for review. That is, we regard the act of submitting the IP Log for review as this assertion.

The EDP also defines a project lead role: project leads are responsible for ensuring that the processes are being followed. So, for example, project leads need to ensure that committers are engaging in the IP due diligence process. But they are also responsible for ensuring that committers are “playing nice”. That is, they need to ensure that the level playing field and vendor-neutrality bits in the EDP are being observed, but the EDP does not explicitly grant project leads any special powers to make technical decisions on behalf of the project team.

As the first link in the project leadership chain, one important power granted to project leads by the EDP is the ability to retire committers who are working against the interests of the team. This is essentially the nuclear option (per Merriam-Webster, “an extreme option regarded as a drastic step or last resort”) and it should be used with care.

[[roles-pmc]]
== Project Management Committee

A project management committee (PMC) is responsible for the operation of exactly one top-level project as defined by the {edpLink} (EDP). Per the EDP, top-level projects sit at the top of the open source project hierarchy. 

Top-level projects do not generally maintain open source code of their own, but rather provide oversight and guidance to those open source projects that fall under them in the project hierarchy. All projects that fall under a particular top-level project must fit within the mission and scope defined by the top-level project’s charter. In addition to mission and scope, the charter may further define other requirements or establish guidelines for practices by the project that fall under its purview.

The primary role of the PMC is to ensure that project teams are implementing the EDP and operating within the terms outlined by the top-level project's charter. In particular, the PMC monitors project activity to ensure that project teams are operating in an open and transparent manner. 

The PMC is responsible for defining and managing the structure of the top level project and the organization of the open source (software and specification) projects contained within.

The PMC provides other oversight regarding the operation of open source projects. They review and approve release and progress review materials, and facilitate cross-project communication within the top level project.

=== Composition

A PMC has one or more PMC leads and zero or more PMC Members. Together the PMC provides oversight and overall leadership for the projects that fall under their top Level project. The PMC as a whole, and the PMC leads in particular, are ultimately responsible for ensuring that the Eclipse Development Process (EDP) is understood and followed by their projects. The PMC is additionally responsible for maintaining the top level project's charter.

The PMC’s role is, fundamentally, to maintain the overall vision and operation of the top level project and all of the projects that fall within its purview. Very often (though it really depends on the nature of the top level project), the PMC will take a very active role in determining how its projects fit together, and how they communicate and interoperate. In pragmatic terms, the PMC ensures that projects are following the rules, set the standards over and above the basic requirements for releases and corresponding documentation, approves intellectual property contributions, and approves committer and project lead elections. 

The PMC Lead is appointed by the Eclipse Foundation's Board of Directors. When an existing PMC Lead retires, the EMO will work with the PMC to identify a replacement and then with the EMO(ED) to have that individual ratified by the board of directors.

PMC Members are appointed by the EMO(ED). The criteria by which PMC members are identified varies by PMC, but is generally by nomination and election within the existing PMC. Some PMCs are designed to be composed of representatives from some subset of the projects that operate under their purview. In such a case, the means of identifying members should be documented in the corresponding top-level project charter.

=== PMC Role in the Intellectual Property Due Diligence Process

The Eclipse IP Team has limited technical knowledge and so relies on the PMC to use their relationship with the open source projects operating under their purview, their knowledge and experience with the community and related technology to provide technical insight. Specifically, the IP Team depends on the PMC to use their knowledge and experience with their community and technology to flag any potential issues that may not be obvious.

The PMC might, for example, know that the license for some content had been, at some point in the past, changed; or that some particular piece of content links in some non-obvious manner to other intellectual property that should also be vetted.

PMC members should highlight issues directly on the CQ. The IP Team interprets a `pass:[+1]` as _no known issues_.

[TIP]
====
The PMC should not perform any detailed analysis of content. The IP Team will engage in detailed analysis, identify license compatibility issues, and determine whether or not license headers have been appropriately applied, for example.
====


In part, PMC approval of an intellectual property contribution (a <<ip-cq,contribution questionnaire>> or CQ) means that the PMC believes that the addition of the contribution to the project makes technical sense, but from an intellectual property perspective (that is, does it make sense to incorporate this intellectual property into the project?) 



At least in part, PMC approval is required to ensure that the PMC is actively engaged with the project that operate under their purview in the event that the EMO requires some assistance working with the project (the PMC member that approves a CQ becomes an obvious contact should the IP Team require your assistance). 



Some PMCs make additional requirements; we can discuss what those additional requirements might be, but I recommend that you defer considering that until a need is demonstrated.

==== Project Content

Is the contribution in scope.

==== Third Party Content

=== PMC Role in Elections

The PMC reviews and approves (or vetos) committer elections, first validating that candidate committers have demonstrated sufficient merit. 

PMC members can review elections that require their attention on the _Notifications_ page. 

[TIP]
====
Click the drop-down next to "Welcome, <name>" to access the Notifications page.

image::images/notifications.png[]
====

=== PMC Role in Reviews

Literally put a `pass:[+1]` on the email thread when approving a review.

=== PMC Role in Grievance Handling

The PMC is a link in the project leadership chain. As such, the PMC has a role in the grievance handling process: they identify and document project dysfunction (with the responsibility to remove or replace disruptive committers).

=== The PMC Mailing list

We set the "pmc" list as the "dev" list for top level projects. This list is the primary means that the EMO and the Eclipse Foundation processes (e.g. committer elections) use to interact with the PMC.

The PMC members should all be subscribed to that list.

Over time, at least one committer/project lead from each subproject will also be subscribed to that list, making the list a good way for the EMO to interact with the entire project community under that TLP.



The PMC is an important link in the project leadership chain, which is composed of the project's project lead(s), the leadership of the parent project (if any), the PMC leads and PMC members, the EMO, and the Executive Director of the Eclipse Foundation. In exceptional situations—such as projects with zero active committers, disruptive committers, or no effective project leads—the project leadership chain has the authority to make changes (add, remove) to the set of committers and/or project leads of that project, and otherwise act on behalf of the project lead.

PMC Leads are are not elected. They are vetted by the EMO, approved by the Eclipse Board of Directors, and appointed by the Executive Director of the Eclipse Foundation. PMC members are elected by the existing PMC leads and members, and approved by the Executive Director of the Eclipse Foundation.

In the unlikely event that a member of the PMC becomes disruptive to the process or ceases to contribute for an extended period, the member may be removed by the unanimous vote of the remaining PMC members, subject to approval by the EMO. Removal of a PMC Lead requires approval of the Board.

Every PMC is entitled to appoint one representative to each of the Eclipse Architecture and Planning Councils. The Architecture Council’s main roles are to provide general architectural oversight for projects, and maintain the EDP. In particular, a representative of EE4J on the Architecture Council could advocate for modifications to the Eclipse Development Process if there are requirements that are counter-productive for a large runtime project such as EE4J.

== Frequently Asked Questions

[qand]

Is the Project Lead a super-committer? ::

A project lead is not a super committer or anything like that. Committer and project lead are two different roles. A project lead is almost always also a committer. Most privileges on an Eclipse open source project are given to committers. I say most privileges, because we're always trying to sort out how to give project leads more privileges in GitHub and GitLab (e.g., Bug 533471).

